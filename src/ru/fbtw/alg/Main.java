package ru.fbtw.alg;

import ru.fbtw.alg.core.BigDeciminalSolution;
import ru.fbtw.alg.core.DoubleSolution;
import ru.fbtw.alg.core.Solution;

import java.util.Locale;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Locale.setDefault(Locale.ROOT);

		Solution solution;
		Scanner in = new Scanner(System.in);

		System.out.println("Желаете ли делать вычисления повышенной точности? (y = 1; n = 0)");

		if (in.nextInt() == 0) {
			solution = new DoubleSolution(in);
		} else {
			solution = new BigDeciminalSolution(in);
		}

		solution.execute();

	}
}
