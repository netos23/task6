package ru.fbtw.alg.core;

import java.util.Scanner;

public class DoubleSolution implements Solution<Double> {

	private double x;
	private double e;
	private int n;

	private DoubleSolution(double x, double e, int n) {
		this.x = x;
		this.e = e;
		this.n = n;
	}

	public DoubleSolution(Scanner in) {
		System.out.println("Введите x из отрезка [-1;1]");
		this.x = in.nextDouble();

		System.out.println("Введите e");
		this.e = in.nextDouble();

		System.out.println("Введите n");
		this.n = in.nextInt();
	}

	/**
	 *
	 * Сумма первых n членов последовательности 1 - 3x + 9x^2 - 27x^3....
	 *
	 * */
	@Override
	public Double mathFuncByN() {
		double res = 1;
		double prev = 1;

		for (int i = 1; i < n; i++) {
			prev = getMemberByT(prev);
			res += prev;
		}
		return res;
	}

	@Override
	public Double mathFuncByE() {
		double res = 1;
		double prev = 1;

		while (Math.abs(prev) > e) {
			prev = getMemberByT(prev);
			System.out.println(prev);
			res += prev;
		}
		return res;
	}

	@Override
	public Double getClassicMathFunc() {
		return 1.0 / (1.0 + 3.0 * x);
	}

	@Override
	public void execute() {
		System.out.printf("Cумма n слагаемых: %f\n", mathFuncByN());
		System.out.printf("Cумма слагаемых меньшиз е: %f\n", mathFuncByE());
		System.out.printf("Cумма слагаемых меньшиз е/10: %f\n", mathFuncByE(e / 10));
		System.out.printf("Значение через класические средства: %f\n", getClassicMathFunc());

	}


	@Override
	public Double getMemberByT(Double previous) {
		return -3 * previous * x;
	}

	@Override
	public Double getX() {
		return x;
	}

	@Override
	public void setX(Double x) {
		this.x = x;
	}

	@Override
	public Double getE() {
		return e;
	}

	@Override
	public void setE(Double e) {
		this.e = e;
	}

	@Override
	public int getN() {
		return n;
	}

	@Override
	public void setN(int n) {
		this.n = n;
	}


}
