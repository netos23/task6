package ru.fbtw.alg.core;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Scanner;

public class BigDeciminalSolution implements Solution<BigDecimal> {
	private static final int precision = 50;

	// Коэфицент в разложенной формуле
	private static final BigDecimal K = BigDecimal.valueOf(-3);
	// Коэфицент в краткой формуле
	private static final BigDecimal F = BigDecimal.valueOf(3);
	// Коэфицент для вычисления e/10
	private static final BigDecimal M = BigDecimal.valueOf(10);

	private BigDecimal x;
	private BigDecimal e;
	private int n;

	private MathContext context;


	public BigDeciminalSolution(BigDecimal x, BigDecimal e, int n) {
		context = new MathContext(precision, RoundingMode.HALF_UP);

		this.x = x;
		this.e = e;
		this.n = n;

	}

	public BigDeciminalSolution(double x, double e, int n) {
		context = new MathContext(precision, RoundingMode.HALF_UP);

		this.x = new BigDecimal(x, context);
		this.e = new BigDecimal(e, context);
		this.n = n;
	}

	public BigDeciminalSolution(Scanner in) {
		context = new MathContext(precision, RoundingMode.HALF_UP);

		System.out.println("Введите x из отрезка [-1;1]");
		this.x = in.nextBigDecimal();

		System.out.println("Введите e");
		this.e = in.nextBigDecimal();

		System.out.println("Введите n");
		this.n = in.nextInt();
	}

	/**
	 *
	 * Сумма первых n членов последовательности 1 - 3x + 9x^2 - 27x^3....
	 *
	 * */
	@Override
	public BigDecimal mathFuncByN() {
		BigDecimal previous = new BigDecimal("1", context);
		BigDecimal sum = new BigDecimal("1", context);

		for (long i = 1L; i < n; i++) {
			previous = getMemberByT(previous);
			sum = sum.add(previous);
		}

		return sum;
	}

	@Override
	public BigDecimal mathFuncByE() {
		BigDecimal previous = new BigDecimal("1", context);
		BigDecimal sum = new BigDecimal("1", context);
		while (previous.abs().compareTo(e) > 0) {
			previous = getMemberByT(previous);
			sum = sum.add(previous);
		}

		return sum;
	}

	@Override
	public BigDecimal getClassicMathFunc() {
		return BigDecimal.ONE
				.divide(BigDecimal.ONE.add(x.multiply(F)), context);
	}

	@Override
	public void execute() {
		System.out.printf("Cумма n слагаемых: %f\n", mathFuncByN());
		System.out.printf("Cумма слагаемых меньшиз е: %f\n", mathFuncByE());
		System.out.printf("Cумма слагаемых меньшиз е/10: %f\n", mathFuncByE(e.divide(M, context)));
		System.out.printf("Значение через класические средства: %f\n", getClassicMathFunc());
	}


	@Override
	public BigDecimal getMemberByT(BigDecimal previous) {
		return previous.multiply(K).multiply(x);
	}

	@Override
	public BigDecimal getX() {
		return x;
	}

	public void setX(double x) {
		setX(new BigDecimal(x, context));
	}

	@Override
	public void setX(BigDecimal x) {
		this.x = x;
	}

	@Override
	public BigDecimal getE() {
		return e;
	}

	@Override
	public void setE(BigDecimal e) {
		this.e = e;
	}

	public void setE(double e) {
		setX(new BigDecimal(e, context));
	}

	@Override
	public int getN() {
		return n;
	}

	@Override
	public void setN(int n) {
		this.n = n;
	}
}
