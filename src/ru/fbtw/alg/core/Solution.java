package ru.fbtw.alg.core;

public interface Solution<T> {

	/**
	 *
	 * Сумма первых n членов последовательности 1 - 3x + 9x^2 - 27x^3....
	 *
	 * */
	T mathFuncByN();

	default T mathFuncByN(T x, int n) {
		setX(x);
		setN(n);
		return mathFuncByN();
	}


	default T mathFuncByN(int n) {
		setN(n);
		return mathFuncByN();
	}
	/**
	 *
	 * Сумма первых  членов последовательности, меньших e 1 - 3x + 9x^2 - 27x^3....
	 *
	 * */
	T mathFuncByE();

	default T mathFuncByE(T x, T e) {
		setX(x);
		setE(e);

		return mathFuncByE();
	}

	default T mathFuncByE(T e) {
		setE(e);
		return mathFuncByE();
	}

	/**
	 *
	 *  Значение через math
	 *
	 * */
	T getClassicMathFunc();

	default T getClassicMathFunc(T x) {
		setX(x);
		return getClassicMathFunc();
	}


	void execute();

	default void execute(T x, T e, int n) {
		setX(x);
		setE(e);
		setN(n);

		execute();
	}


	T getMemberByT(T previous);

	T getX();

	void setX(T x);

	T getE();

	void setE(T e);

	int getN();

	void setN(int n);

}
